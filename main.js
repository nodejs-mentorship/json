const fs = require("fs");

const person1 = {
  name: "Marko",
  lastName: "Markovic",
  age: 18,
  male: true,
};

const person2 = {
  name: "Pera",
  lastName: "Peric",
  age: 23,
  male: true,
};

const person3 = {
  name: "Milica",
  lastName: "Markovic",
  age: 30,
  male: false,
};

const personsJson = JSON.stringify([person1, person2, person3]);

fs.writeFileSync("persons.json", personsJson);
